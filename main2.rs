use actix_web::{
    get,
    web::{self, Data},
    App,
    HttpResponse, 
    HttpServer, 
    Responder,
    middleware::Logger
};


use std::{
    env,
    thread,
    time::SystemTime,
    process::Command,
    time::UNIX_EPOCH,
    process::exit,
};
use std::sync::atomic::{AtomicUsize, Ordering};

use env_logger;
use paho_mqtt as mqtt;
use mqtt::{AsyncClient, message};
extern crate rmp;

static GLOBAL_ID_COUNT: AtomicUsize = AtomicUsize::new(0);

fn print(tempo: u128){
    let the_command = format!("echo {:?} >> delta.csv", tempo);
    Command::new("/bin/bash")
        .arg("-c")
        .arg(the_command.clone())
        .output()
        .expect(":: Failed to run the command ::");

}

#[get("/http/{topic}/{message}")]
async fn trigger(path: web::Path<(String, String)>, client: Data<AsyncClient>) -> impl Responder {

    let (topic, message) = path.into_inner();

    client.subscribe(topic.clone(), 0).wait().unwrap();
    let uid = i128::try_from(GLOBAL_ID_COUNT.fetch_add(1, Ordering::SeqCst)).unwrap();

    //Tempo Iniziale
    let tempo_iniziale = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_micros();

    let tempo_string = format!("{}", tempo_iniziale);
    
    let message = format!("{}#{}#{}", message.clone(), tempo_string, uid);

    let mut buff = Vec::new();
    rmp::encode::write_str(&mut buff, message.clone().as_str()).unwrap_or_else(|e|{
        eprintln!("Impossibile preparare il buffer: {:?}", e);
        client.disconnect(None);
        exit(1)
    });

    let msg = mqtt::Message::new(topic.clone(), buff, 0);

    client.publish(msg).wait().unwrap();
   
    HttpResponse::Ok().body("Ok")
    
}

async fn thread_risposte(mut client: mqtt::AsyncClient){
    println!("Thread risposte");
    client.subscribe("output", 0).wait().unwrap();
    let strm = client.get_stream(1024);

    while let Some(msg) = strm.recv().await.unwrap(){

        println!("{}", msg.clone());
        let payload = msg.payload(); // it's a slice
        let decoded_payload = rmp::decode::read_str_from_slice(payload).unwrap_or_else(|e| {
            eprintln!("T::O::   errore decodifica: {:?}", e);
            exit(1);
        });
        let decoded = decoded_payload.0.to_string();
        
        let mut split = decoded.split('#');
        split.next();
        
        let tempo_finale = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros();

        let dig = split.next().unwrap().trim();
        let tempo_iniziale:u128 = dig.parse().unwrap();

        let difference = (tempo_finale - tempo_iniziale)/1000;
        
        //println!("{}", difference);
        print(difference);

    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let triggerport = option_env!("TRIGGERPORT").or(Some("8081")).unwrap();
    let port = triggerport.parse::<u16>().unwrap_or(8080);
    let server = env::var("SERVER").unwrap_or("127.0.0.1".to_string());
    let port_c = env::var("PORT").unwrap_or("1883".to_string());
    let indirizzo = format!("tcp://{}:{}", server, port_c);
    
    let cli = AsyncClient::new(indirizzo.clone()).unwrap();
    let conn_opts = mqtt::ConnectOptionsBuilder::new().finalize();
    cli.connect(conn_opts.clone()).wait()?;

    let cli_s = mqtt::AsyncClient::new(indirizzo.clone()).unwrap();
    cli_s.connect(conn_opts.clone()).wait()?;

    tokio::spawn(async move {
        thread_risposte(cli_s).await;
    });
    
    HttpServer::new(move || {
        App::new()
            .data(cli.clone())
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
            .service(trigger)
    })
    .bind(("0.0.0.0",port))?
    .run()
    .await

}